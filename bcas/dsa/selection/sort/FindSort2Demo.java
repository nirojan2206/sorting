package bcas.dsa.selection.sort;

public class FindSort2Demo {
	
	
	public static void main(String[] args) {
		
		FindSort2 s = new FindSort2();
		int arr[] = { 5, 2, 1, 3, 6, 4, 7};
		
		System.out.print("Before Sort: ");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	
		FindSort2.selectionSort(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
	
	
	
	}
