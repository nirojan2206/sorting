package bcas.dsa.selection.sort;

public class FindSort {

		public static void findSorted(int[] arr) {
			int n = arr.length;
			int swap = 0;
			for (int i = 0; i < n; i++) {
				int index = i;
				int min = arr[i];
				for (int j = i + 1; j < n; j++) {
					if (arr[j] < arr[index]) {
						index = j;

					}
				}
				swap = arr[index];
				arr[index] = arr[i];
				arr[i] = swap;

			}
		}

		public static void main(String[] args) {
			int arr[] = { 5, 2, 1, 3, 6, 4, 7, 2, 9, 10};
			System.out.print("Before Sort: ");
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
			findSorted(arr);

			System.out.println();
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}

		}

	}

