package bcas.dsa.selection.sort;

public class FindSort2 {
	
	public static int [] selectionSort (int [] arr) {
		
		for (int i = 0; i<arr.length - 1; i++) {
			
			int index = i, swap =0;
	
		for (int j = i; i<arr.length; j++) {
			
			if (arr[j]<arr[index]) {
				
				index = j;
			}
		}
		
		swap = arr [index];
		arr [index] = arr [i];
		arr[i] = swap;
		
		
		}
		return arr;
	}

}
