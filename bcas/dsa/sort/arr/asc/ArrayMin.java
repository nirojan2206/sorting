package bcas.dsa.sort.arr.asc;

import java.util.Arrays;
//import java.util.Random;

public class ArrayMin {

	/*public static void createRanArray(int size , int range ) {
		
		Random ran = new Random();
		int [] numArray = new int [size];
			
		for (int i = 0; i<size; i++) {
			numArray[i] = ran.nextInt(range);
		}
	
		}
	*/	

	public int arrayMin(int numArray[]) {

		int value = numArray[0];
		for (int i = 0; i < numArray.length; i++) {
			if (value > numArray[i]) {
				value = numArray[i];
			}
		}
		return value;

	}

	public int arrayIndex(int numArray[]) {

		int value = numArray[0];
		int index = 0;
		for (int i = 0; i < numArray.length; i++) {
			if (value > numArray[i]) {
				value = numArray[i];
				index = i;
			}
		}
		return index;

	}

	public void arrayAsc(int numArray[]) {
		int vall = 0;

		for (int i = 0; i < numArray.length - 1; i++) {
			for (int j = i + 1; j < numArray.length; j++) {

				if (numArray[i] > numArray[j]) {

					vall = numArray[i];
					numArray[i] = numArray[j];
					numArray[j] = vall;
				}

			}
		}

		System.out.println(Arrays.toString(numArray));
	}

	public static void printArray(int[] numArray) {

		for (int befSort : numArray) {
			System.out.print(befSort + ",");
		}
	}

}
