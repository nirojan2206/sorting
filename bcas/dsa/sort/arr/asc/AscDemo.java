package bcas.dsa.sort.arr.asc;

public class AscDemo {

	public static void main(String[] args) {
		
		
		int [] numArray = {05,15,10,25,20,30,02,01,12,10,12,03};
		
		ArrayMin a = new ArrayMin();
		System.out.println(a.arrayMin(numArray));
		System.out.println(a.arrayIndex(numArray));
		a.arrayAsc(numArray);
		a.printArray(numArray);
		//a.createRanArray(10, 20);
	}

}


/*
 * 
 * Before Sorting : 5,15,10,25,20,30,2,1,12,10,12,3,
 * 
 * 
 * After Sorting : 
*/