package bcas.dsa.sort.arr.asc2;

public class SortedArrayDemo {

	public static void main(String[] args) {
		
		int unSortedArray[] = { 20, 10, 20, 70, 90, 30 };

		SortedArrayAPI sortAPI = new SortedArrayAPI();
		
//		System.out.println("Minimum value is : " + sortAPI.findMinimumValue(unSortedArray));
//		System.out.println("Minimum index is : " + sortAPI.findMinimumIndex(unSortedArray));
		
		int size = DemoUtil.readUserInput("Enter the size of array");
		int range = DemoUtil.readUserInput("Enter the random range");
		
		int unSortedRandomArray[]=DemoUtil.generateRandomArray(size, range);
		
		System.out.print("Array befor sort : ");
		DemoUtil.printArray(unSortedRandomArray);	
		
		System.out.print("\nArray after sort : ");
		DemoUtil.printArray(sortAPI.sortArray(unSortedRandomArray));
	
	}

}

/*


before sorting :

after sorting:
*/