package bcas.dsa.sort.arr.asc2;

import java.util.Random;
import java.util.Scanner;

public class DemoUtil {

	public static void printArray(int numArray[]) {
		for (int i : numArray) {
			System.out.print(i + " ");
		}
	}
	

	public static int[] generateRandomArray(int size, int range) {
		int numArray[] = new int[size];
		Random rand = new Random();
		for (int index = 0; index < size; index++) {
			int randNum = rand.nextInt(range);
			randNum= (randNum > 0) ? (numArray[index] = randNum) : index--;
			
			
		}
			
		return numArray;
	}
	


	public static int readUserInput(String msg) {
		System.out.print(msg + " : ");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}
}
