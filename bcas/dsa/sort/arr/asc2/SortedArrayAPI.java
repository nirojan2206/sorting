package bcas.dsa.sort.arr.asc2;

public class SortedArrayAPI {
	private PositionAndValue pnv = null;
	private int sortedArray[];

	public int[] sortArray(int[] numArray) {
		int index=0;
		sortedArray = new int[numArray.length];
		while (isArryFilled()) {
			findMinimumValueAndIndex(numArray);
			numArray[pnv.getIndex()] = 0;
			sortedArray[index] = pnv.getValue();
			index++;
		}
		return sortedArray;
	}

	public int findMinimumValue(int numArray[]) {
		int min = numArray[0];
		for (int i = 1; i < numArray.length; i++) {
			if (min > numArray[i]) {
				min = numArray[i];
			}
		}
		return min;
	}

	public int findMinimumIndex(int numArray[]) {
		int min = numArray[0];
		int index = 0;
		for (int i = 1; i < numArray.length; i++) {
			if (min > numArray[i]) {
				min = numArray[i];
				index = i;
			}
		}
		return index;
	}

	private PositionAndValue findMinimumValueAndIndex(int numArray[]) {
		pnv = new PositionAndValue();
		int min = 0, index = 0, j = 0;
		while (j < numArray.length) {
			if (numArray[j] != 0) {
				min = numArray[j];
				break;
			}
			j++;
		}

		for (int i = 1; i < numArray.length; i++) {
			if (numArray[i] != 0 && min >= numArray[i]) {
				min = numArray[i];
				index = i;
			}
		}
		pnv.setValue(min);
		pnv.setIndex(index);
		return pnv;
	}

	private boolean isArryFilled() {
		if (sortedArray[sortedArray.length - 1] == 0) {
			return true;
		}
		return false;
	}
}
