package bcas.dsa.sort.arr.asc2;

public class PositionAndValue {

	private int value;
	private int index;
	
	public PositionAndValue() {
		
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return "PositionAndValue [value=" + value + ", index=" + index + "]";
	}	
}
